package controllers;

import controllers.Controller;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.File;
//app
public class Main extends Application {
    public static File file;
    @Override
    public void start(Stage stage) throws Exception{
        String fxmlFile = "/view/sample.fxml";
        FXMLLoader loader = new FXMLLoader();
        Parent root =loader.load(getClass().getResourceAsStream(fxmlFile));
        stage.setTitle("JavaFX and Maven");
        //stage.initStyle(StageStyle.TRANSPARENT);           УБРАТЬ ВИНДОС МЕНЮ
        //scene.setFill(Color.TRANSPARENT);            ПРОЗРАЧНОСТЬ НО НАДО КАК-ТО ПРИВЯЗАТЬ К СЦЕНЕ
        stage.setScene(new Scene(root));
        stage.show();


    }


    public static void main(String[] args) {
        launch(args);
    }
}
