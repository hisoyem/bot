package controllers;

import java.util.prefs.Preferences;
//app
public class SaveSettings {
    private int DefaultInt;
    private Preferences prefs;
    SaveSettings(){
        prefs = Preferences.userRoot().node(this.getClass().getName());
    }
    SaveSettings(int DefaultInt){
        prefs = Preferences.userRoot().node(this.getClass().getName());
        this.DefaultInt=DefaultInt;
    }

    public void putString(String key,String value){
        prefs.put(key,value);
    }

    public String getString(String key){
        return prefs.get(key,"Default");
    }

    public void putInt(String key,int value){
        prefs.putInt(key,value);
    }

    public int getInt (String key){
        return prefs.getInt(key,DefaultInt);
    }
}
