package controllers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.ArrayList;
//app
public class TestProxy extends Thread {

    private String proxy;

    public TestProxy(String proxy){
        this.proxy=proxy;
        run();
    }


    @Override
    public void run() {
        if(!proxy.equals("")) {
            try {
                System.setProperty("webdriver.chrome.driver", "C:\\Users\\Xiaomi\\Desktop\\chromedriver.exe");
                ChromeOptions options= new ChromeOptions();
                options.addArguments("--proxy-server="+proxy);
                options.addArguments("--fast-start");
                options.addArguments("--force-first-run");
                ChromeDriver driver = new ChromeDriver(options);
                long s = System.currentTimeMillis();
                driver.get("https://yandex.ru/internet/");
                long e = System.currentTimeMillis();
                System.out.println((e - s) / 1000);

            } catch (Exception e) {
                System.out.println(e);
                System.out.println("Error with proxy or check proxy filed");
            }
        }
        else {
            System.out.println("Proxy filed is empty");
        }
    }
}
